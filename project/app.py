from flask import Flask, render_template, request
from werkzeug.utils import secure_filename

app = Flask(__name__)
ALLOWED_EXTENSIONS = {'ged'}
app.config['UPLOAD_FOLDER'] = './gedcom'

@app.route('/')
def page_main():
    return render_template('index.html', title='familytrees51')


@app.route('/gedcom_upload', methods=['POST'])
def gedcom_upload():
    if request.method == 'POST':
        f = request.files['file']
        f.save(secure_filename(f.filename))
        return 'file uploaded successfully'
    return "HERE"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
