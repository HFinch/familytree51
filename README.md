# FamilyTree51

## Docker container
starting docker container with ``docker-compose up -d`` or use pycharms service to do that.

To access the website type ``http://localhost:9999``.

## Informations

Project code is located under `./project`.

`./static` contains all the static files like `js` or `css` files.

``./templates`` contains the html templates to be rendered.